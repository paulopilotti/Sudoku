package sudoku;

public class Sudoku implements sudokuInterface {

	SudokuData data = new SudokuData(new int[][] {
			{8,3,0,0,0,5,6,9,0},
            {0,0,6,0,8,0,0,0,2},
            {0,0,0,6,0,0,0,0,5},
            {6,0,0,0,0,3,0,0,0},
            {3,0,5,0,0,0,9,0,6},
            {0,0,0,9,0,0,0,0,7},
            {4,0,0,0,0,2,0,0,0},
            {5,0,0,0,4,0,1,0,0},
            {0,8,7,1,0,0,0,4,9}
            });

	@Override
	public void imprimeJogoInicial() {

		  int linha, coluna;
		  System.out.println("0 = Espaço para solução \n");
		  System.out.println("Jogo inicial");
		  for (linha = 0; linha < 9; linha++) {
		    for (coluna = 0; coluna < 9; coluna++) {
		    	System.out.print(data.tabela[linha][coluna]);
		      if (coluna % 3 == 2) System.out.print("  ");
		    }
		    System.out.println();
		    if (linha % 3 == 2) System.out.print("\n");
		  }
		}

	/* (non-Javadoc)
	 * @see sudoku.sudokuInterface#imprimeTabela()
	 */
	@Override
	public void imprimeJogoResolvido() {

		  int linha, coluna;

		  System.out.println("Solução");
		  for (linha = 0; linha < 9; linha++) {
		    for (coluna = 0; coluna < 9; coluna++) {
		    	System.out.print(data.tabela[linha][coluna]);
		      if (coluna % 3 == 2) System.out.print("  ");
		    }
		    System.out.println();
		    if (linha % 3 == 2) System.out.print("\n");
		  }
		}

	/* (non-Javadoc)
	 * @see sudoku.sudokuInterface#verificaJogo(int, int, int)
	 */
	@Override
	public Boolean verificaJogo(int lin, int col, int n) {

		  int linha, coluna, linhaResolvida, colunaResolvida;

		  if (data.tabela[lin][col] == n) return true;
		  if (data.tabela[lin][col] != 0) return false;
		  for (coluna = 0; coluna < 9; coluna++)
		    if (data.tabela[lin][coluna] == n) return false;
		  for (linha = 0; linha < 9; linha++)
		    if (data.tabela[linha][col] == n) return false;
		  linhaResolvida = lin / 3;
		  colunaResolvida = col / 3;
		  for (linha = linhaResolvida * 3; linha < (linhaResolvida + 1) * 3; linha++)
		    for (coluna = colunaResolvida * 3; coluna < (colunaResolvida + 1) * 3; coluna++)
		      if (data.tabela[linha][coluna] == n) return false;
		  return true;
		}


	/* (non-Javadoc)
	 * @see sudoku.sudokuInterface#resolveJogo(int, int)
	 */
	@Override
	public void resolveJogo(int lin, int col) {
		  int n, t;
		  if (lin == 9)
		    imprimeJogoResolvido(); //Se for a última linha da tabela, encerra o jogo e imprime a solução na tela
		  else
		    for (n = 1; n <= 9; n++)
		      if (verificaJogo(lin, col, n)) {
		        t = data.tabela[lin][col];
		        data.tabela[lin][col] = n;
		        if (col == 8)
		          resolveJogo(lin + 1, 0);
		        else
		          resolveJogo(lin, col + 1);
		        data.tabela[lin][col] = t;
		      }
		}
}
