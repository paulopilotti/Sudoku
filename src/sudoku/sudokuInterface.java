package sudoku;

public interface sudokuInterface {

	public void imprimeJogoInicial();
	
	void imprimeJogoResolvido();

	Boolean verificaJogo(int lin, int col, int n);

	void resolveJogo(int lin, int col);

}